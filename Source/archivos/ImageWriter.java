package archivos;

import static com.mongodb.client.model.Filters.eq;

import java.io.FileOutputStream;
import java.io.IOException;

import org.bson.Document;
import org.bson.types.Binary;

public class ImageWriter extends DatabaseInitializer {
	
	public static Document retrieve(String fileName, String filePath)
    {
        Binary c;
        byte d[];
        Document obj = null;
        try
        {
        	obj = collection.find(eq("Name", fileName)).first();
            c = obj.get("Image binary data:", org.bson.types.Binary.class);
            FileOutputStream fout = new FileOutputStream(filePath);
            d = c.getData();
            fout.write(d);
            fout.flush();
            fout.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return obj;
    }
	
	public static void retrieveGridFS(String fileName, String filePath) throws IOException {
		
		FileOutputStream streamToDownloadTo = new FileOutputStream(filePath);
		gridFSBucket.downloadToStream(fileName, streamToDownloadTo);
		streamToDownloadTo.close();
		System.out.println(streamToDownloadTo.toString());
	}

}
